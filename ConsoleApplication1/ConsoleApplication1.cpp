﻿
#include <iostream>
#include <vector>
#include <ctime>
#include <cstdlib>
#include <algorithm>

using namespace std;

class SortingArray //создание класса SortingArray
{
	vector<int> arr;

public :
	SortingArray() { //заполняем массив (10 элементов) рандомными значениями 
		for (int i = 0; i < 10; i++) {
			arr.push_back(rand() % 20);
		}
	}
	int get(int index) { //get(int index) возвращающий элемент массива с индексом index
		return arr[index];
	}

	vector<int>* sortArr() { //сортируем массив и возвращаем его отсортированную копию
		vector<int>* sorted = new vector<int>;
		copy(arr.begin(), arr.end(), back_inserter(*sorted));
		std::sort(sorted->begin(), sorted->end());
		return sorted;
	}

	void print() { //функция вывода массива (неотсортированного)
		for (int i = 0; i < 10; i++) {
			cout << arr[i] << " ";
		}
		cout << endl;
	}
};


class I_Medians //пользовательский интерфейс
{

public: //набор абстрактных методов
	virtual void print() = 0;
	virtual int get_median(int digit) = 0;

};

template <typename T>

class C_Medians:public I_Medians{ //реализация и адаптация интерфейса к классу SortingArray
	T* arr;
public:

	C_Medians() { //конструктор
		arr = new T();
	}

	void print() { //реализация вывода
		arr->print();
	}

	int get_median(int digit) { //реализация digit-a
		vector<int> sorted = *(arr->sortArr());
		return sorted[digit];
	}
	
};

class MedianBuilder {
public: 
	static I_Medians* getMedian() {
		I_Medians* newmedian = new C_Medians<SortingArray>();
		return newmedian;
	}
};

int main()
{
	setlocale(LC_ALL, "Russian");
	I_Medians* test = MedianBuilder::getMedian();
	test->print();
	int digit;
	cout << "Введите индекс: ";
	cin >> digit;
	cout << test->get_median(digit);
}

